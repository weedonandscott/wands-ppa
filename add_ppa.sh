#!/bin/bash

curl -s --compressed https://ppa.weedonandscott.com/KEY.gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/wands.gpg >/dev/null
curl -s --compressed -o /etc/apt/sources.list.d/wands.list https://ppa.weedonandscott.com/wands.list
